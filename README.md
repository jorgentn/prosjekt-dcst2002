# Game Rating

## How to start application:

## From zip delivered in inspera:

### Unzip file and open it in VScode

```sh
cd server
npm start
```

```sh
cd client
npm start
```

### Go to http://localhost:3000/#/ in web browser

### You need to register a user and log in, to rate and review games.

###

## From git repository:

[lenken til git repository](https://gitlab.stud.idi.ntnu.no/jorgentn/prosjekt-dcst2002.git)

## Setup database connections

You need to create two configuration files that will contain the database connection details. An
example content of the two configuration files

`server/config.ts`:

```ts
process.env.MYSQL_HOST = 'mysql.stud.ntnu.no';
process.env.MYSQL_USER = 'jorgentn_todo';
process.env.MYSQL_PASSWORD = 'jorgentn_todo';
process.env.MYSQL_DATABASE = 'jorgentn_prosjekt_dev';
```

`server/test/config.ts`:

```ts
process.env.MYSQL_HOST = 'mysql.stud.ntnu.no';
process.env.MYSQL_USER = 'username_todo';
process.env.MYSQL_PASSWORD = 'username_todo';
process.env.MYSQL_DATABASE = 'username_todo_test';
```

These environment variables will be used in the `server/src/mysql-pool.ts` file.

## Database

The database must have these tables in order to ensure that the site works as required

```sql
CREATE TABLE review (
   review_id INT PRIMARY KEY AUTO_INCREMENT,
   id INT,
   title Varchar(32),
   review VARCHAR(1024),
   rating INT,
   helpful INT,
   not_helpful INT,
   user_name VARCHAR(64)
);

CREATE TABLE rating (
   id INT PRIMARY KEY,
   rating_total INT,
   rating_count INT
);

CREATE TABLE users (
id INT PRIMARY KEY AUTO_INCREMENT,
username varchar(32) NOT NULL,
passord varchar(64) NOT NULL
);

create TABLE user_like (
   id INT PRIMARY KEY AUTO_INCREMENT,
   user_id INT NOT NULL,
   review_id INT NOT NULL,
   helpful boolean,
   not_helpful boolean
   FOREIGN KEY (review_id)
        REFERENCES review(review_id)
        ON DELETE CASCADE
);

```

## Start server

Install dependencies and start server:

```sh
cd server
npm install
npm start
```

### Run server tests:

```sh
cd server
npm test
```

## Bundle client files to be served through server

Install dependencies and bundle client files:

```sh
cd client
npm install
npm start
```

### Run client tests:

```sh
cd client
npm test
```
