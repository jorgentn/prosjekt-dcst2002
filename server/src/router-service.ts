import pool from './mysql-pool';

export type Rating = {
  id: number;
  rating_total: number;
  rating_count: number;
};

export type Review = {
  review_id: number;
  id: number;
  title: string;
  review: string;
  rating: number;
  helpful: number;
  not_helpful: number;
  user_name: any;
  state: boolean;
};

export type Like_User = {
  id: number;
  user_id: number;
  review_id: number;
  helpful: boolean;
  not_helpful: boolean;
};

class RouterService {
  /**
   * Get task with given id.
   */
  getRating(id: number) {
    return new Promise<any>((resolve, reject) => {
      pool.query('SELECT * FROM rating WHERE id = ?', [id], (error, results) => {
        if (error) return reject(error);

        resolve(results);
      });
    });
  }

  getReviews(id: number) {
    return new Promise<any>((resolve, reject) => {
      pool.query('SELECT * FROM review  WHERE id = ? ', [id], (error, results) => {
        if (error) return reject(error);

        resolve(results);
      });
    });
  }

  getTopReview(id: number) {
    return new Promise<Review>((resolve, reject) => {
      pool.query(
        'SELECT * FROM review  WHERE id = ? ORDER BY helpful DESC',
        [id],
        (error, results) => {
          if (error) return reject(error);

          resolve(results[0]);
        }
      );
    });
  }

  getReview(review_id: number) {
    return new Promise<Rating>((resolve, reject) => {
      pool.query('SELECT * FROM review  WHERE review_id = ?', [review_id], (error, results) => {
        if (error) return reject(error);

        resolve(results);
      });
    });
  }

  getLike_User(user_id: number) {
    return new Promise<Like_User>((resolve, reject) => {
      pool.query('SELECT * FROM user_like WHERE user_id = ?', [user_id], (error, results) => {
        if (error) return reject(error);

        resolve(results);
      });
    });
  }
  Review_Userlike(review_id: number) {
    return new Promise<Like_User>((resolve, reject) => {
      pool.query(
        'SELECT u.helpful, u.not_helpful,r.id AS Game_id, r.helpful AS TotalHelpful, r.not_helpful AS TotalNotHelpful, u.review_id AS ReviewId FROM user_like u, review r WHERE u.review_id = r.review_id AND u.review_id = ?',
        [review_id],
        (error, results) => {
          if (error) return reject(error);

          resolve(results);
        }
      );
    });
  }
  UpdateLike_User(review_id: number, helpful: boolean, not_helpful: boolean) {
    return new Promise<void>((resolve, reject) => {
      pool.query(
        'UPDATE user_like SET helpful = ?, not_helpful = ? WHERE review_id = ?',
        [helpful, not_helpful, review_id],
        (error, results) => {
          if (error) return reject(error);

          resolve(results.affectedRows);
        }
      );
    });
  }
  CreateLike_User(user_id: number, review_id: number, helpful: boolean, not_helpful: boolean) {
    return new Promise<void>((resolve, reject) => {
      pool.query(
        'INSERT INTO user_like (user_id, review_id, helpful, not_helpful) VALUES (?, ?, ?, ?)',
        [user_id, review_id, helpful, not_helpful],
        (error, results) => {
          if (error) return reject(error);

          resolve(results.insertId);
        }
      );
    });
  }

  createRating(id: number, rating: number) {
    return new Promise<number>((resolve, reject) => {
      pool.query(
        'INSERT INTO rating (id, rating_total, rating_count) VALUES (?, ?, 1)',
        [id, rating],
        (error, results) => {
          if (error) return reject(error);

          resolve(Number(results.insertId));
        }
      );
    });
  }

  createReview(review: Review) {
    return new Promise<void>((resolve, reject) => {
      pool.query(
        'INSERT INTO review (id, title, review, rating, helpful, not_helpful, user_name) VALUES (?, ?, ?, ?, 0, 0, ?)',
        [review.id, review.title, review.review, review.rating, review.user_name],
        (error, results) => {
          if (error) return reject(error);

          resolve();
        }
      );
    });
  }

  deleteReview(id: number) {
    return new Promise<void>((resolve, reject) => {
      pool.query('DELETE FROM review WHERE review_id = ?', [id], (error, results) => {
        if (error) return reject(error);

        resolve();
      });
    });
  }

  updateRating(rating: any) {
    return new Promise<void>((resolve, reject) => {
      pool.query(
        'UPDATE rating SET rating_total=?, rating_count=? WHERE id=?',
        [rating.rating_total, rating.rating_count, rating.id],
        (error, results) => {
          if (error) return reject(error);

          resolve();
        }
      );
    });
  }

  updateHelpful(review: Review) {
    return new Promise<void>((resolve, reject) => {
      pool.query(
        'UPDATE review SET helpful=? WHERE review_id=?',
        [review.helpful, review.review_id],
        (error, results) => {
          if (error) return reject(error);

          resolve();
        }
      );
    });
  }

  updateNotHelpful(review: Review) {
    return new Promise<void>((resolve, reject) => {
      pool.query(
        'UPDATE review SET not_helpful=? WHERE review_id=?',
        [review.not_helpful, review.review_id],
        (error, results) => {
          if (error) return reject(error);

          resolve();
        }
      );
    });
  }

  updateReview(review: Review) {
    return new Promise<void>((resolve, reject) => {
      pool.query(
        'UPDATE review SET title=?, review=? WHERE review_id=?',
        [review.title, review.review, review.review_id],
        (error, results) => {
          if (error) return reject(error);

          resolve();
        }
      );
    });
  }

  getAllRatings() {
    return new Promise((resolve, reject) => {
      pool.query('SELECT * FROM rating', (error, results) => {
        if (error) return reject(error);

        resolve(results);
      });
    });
  }
}

const routerService = new RouterService();
export default routerService;
