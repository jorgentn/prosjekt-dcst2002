import express from 'express';
import LoginService from './Login-Services';
import routerService, { Review } from './router-service';
import axios from 'axios';
//@ts-ignore
import bcrypt from 'bcryptjs';
const passport = require('passport');

/**
 * Express router
 */
const router = express.Router();

router.get('/rating', (_request, response) => {
  routerService
    .getAllRatings()
    .then((rows) => response.send(rows))
    .catch((error) => response.status(500).send(error));
});

router.get('/game/:id/top-review', (request, response) => {
  const id = Number(request.params.id);
  routerService
    .getTopReview(id)
    .then((res) => response.send(res))
    .catch((error) => response.status(500).send(error));
});

router.get('/game/:id', (request, response) => {
  const id = Number(request.params.id);

  var ffs = { rating: {}, review: {} };

  routerService
    .getRating(id)
    .then((res) => {
      if (res) {
        ffs.rating = res;
      }
    })
    .catch((error) => response.status(500).send(error));

  routerService
    .getReviews(id)
    .then((res) => {
      if (res) {
        ffs.review = res;
      }
    })
    .then(() => {
      var data = JSON.stringify(ffs);

      response.send(data);
    })
    .catch((error) => response.status(500).send(error));
});

router.get('/like_user/:id', (request, response) => {
  if (request?.params?.id == 'undefined') {
    return;
  } else {
    const user_id = Number(request.params.id);
    routerService.getLike_User(user_id).then((res) => response.send(res));
  }
});

router.delete('/game/:id/delete-review', (request, response) => {
  let num = Number(request.params.id);

  routerService
    .deleteReview(num)
    .then(() => response.send())
    .catch((error) => response.send(error));
});

router.post('/game/:id', (request, response) => {
  const id = Number(request.params.id);
  const rating = Number(request.body.rating);

  routerService
    .createRating(id, rating)
    .then(() => response.send())
    .catch((error) => response.status(500).send(error));
});

router.post('/game/:id/new-review', (request, response) => {
  const review = request.body;

  routerService
    .createReview(review)
    .then(() => response.send())
    .catch((error) => response.status(500).send(error));
});

router.put('/game/:id', (request, response) => {
  const data = request.body;
  const id = request.body.id;
  let newRating: any;

  routerService
    .getRating(id)
    .then((res) => (newRating = res))
    .then(() => newRating[0].rating_count++)
    .then(() => (newRating[0].rating_total += data.rating))
    .then(() => {
      if (data.rating >= 0 && data.rating <= 6)
        routerService
          .updateRating(newRating[0])
          .then(() => response.send())
          .catch((error) => response.status(500).send(error));
      else response.status(400).send('Missing data');
    });
});

router.put('/game/:id/helpful', (request, response) => {
  const review_id = Number(request.params.id);
  const data = request.body;

  let game_id: number;

  if (data.status == 'None') {
    routerService
      .CreateLike_User(data.user_id, review_id, data.helpful, data.nothelpful)
      .then(() => {
        routerService
          .getReview(review_id)
          .then((res: any) => {
            let inc = res[0];
            inc.helpful++;
            routerService.updateHelpful(inc);
            game_id = res[0].id;
          })
          .then(() => {
            routerService.getReviews(game_id).then((res) => response.send(res));
          });
      });
  } else if (data.status == 'Found') {
    routerService
      .Review_Userlike(review_id)
      .then((res: any) => {
        if (data.helpful == true && res[0].not_helpful == false) {
          routerService
            .UpdateLike_User(review_id, data.helpful, data.not_helpful)
            .then(() => {
              let inc = res[0];
              inc.TotalHelpful++;

              let rev: any = { review_id: review_id, helpful: inc.TotalHelpful };
              routerService.updateHelpful(rev);
            })
            .then(() => {
              routerService.getReviews(res[0].Game_id).then((re) => response.send(re));
            });
        } else if (data.helpful == true && res[0].not_helpful == true) {
          routerService
            .UpdateLike_User(review_id, data.helpful, data.not_helpful)
            .then(() => {
              let inc = res[0];
              inc.TotalHelpful++;
              inc.TotalNotHelpful--;

              let revinc: any = { review_id: review_id, helpful: inc.TotalHelpful };
              let revdec: any = { review_id: review_id, not_helpful: inc.TotalNotHelpful };

              routerService.updateHelpful(revinc);
              routerService.updateNotHelpful(revdec);
            })
            .then(() => {
              routerService.getReviews(res[0].Game_id).then((res) => response.send(res));
            });
        } else if (data.helpful == false && res[0].not_helpful == false) {
          routerService
            .UpdateLike_User(review_id, data.helpful, data.not_helpful)
            .then(() => {
              let inc = res[0];
              inc.TotalHelpful--;

              let revinc: any = { review_id: review_id, helpful: inc.TotalHelpful };
              routerService.updateHelpful(revinc);
            })
            .then(() => {
              routerService.getReviews(res[0].Game_id).then((res) => response.send(res));
            });
        }
      })
      .catch((error) => response.status(500).send(error));
  }
});

router.put('/game/:id/nothelpful', (request, response) => {
  const review_id = Number(request.params.id);
  const data = request.body;

  let game_id: number;

  if (data.status == 'None') {
    routerService
      .CreateLike_User(data.user_id, review_id, data.helpful, data.nothelpful)
      .then(() => {
        routerService
          .getReview(review_id)
          .then((res: any) => {
            let inc = res[0];
            inc.not_helpful++;
            routerService.updateNotHelpful(inc);
            game_id = res[0].id;
          })
          .then(() => {
            routerService.getReviews(game_id).then((res) => response.send(res));
          });
      });
  } else if (data.status == 'Found') {
    routerService
      .Review_Userlike(review_id)
      .then((res: any) => {
        if (data.nothelpful == true && res[0].helpful == false) {
          routerService
            .UpdateLike_User(review_id, data.helpful, data.nothelpful)
            .then(() => {
              let inc = res[0];
              inc.TotalNotHelpful++;

              let rev: any = { review_id: review_id, not_helpful: inc.TotalNotHelpful };
              routerService.updateNotHelpful(rev);
            })
            .then(() => {
              routerService.getReviews(res[0].Game_id).then((re) => response.send(re));
            });
        } else if (data.nothelpful == true && res[0].helpful == true) {
          routerService
            .UpdateLike_User(review_id, data.helpful, data.nothelpful)
            .then(() => {
              let inc = res[0];
              inc.TotalHelpful--;
              inc.TotalNotHelpful++;

              let revinc: any = { review_id: review_id, helpful: inc.TotalHelpful };
              let revdec: any = { review_id: review_id, not_helpful: inc.TotalNotHelpful };

              routerService.updateHelpful(revinc);
              routerService.updateNotHelpful(revdec);
            })
            .then(() => {
              routerService.getReviews(res[0].Game_id).then((re) => response.send(re));
            });
        } else if (data.nothelpful == false && res[0].helpful == false) {
          routerService
            .UpdateLike_User(review_id, data.helpful, data.nothelpful)
            .then(() => {
              let inc = res[0];
              inc.TotalNotHelpful--;

              let revdec: any = { review_id: review_id, not_helpful: inc.TotalNotHelpful };
              routerService.updateNotHelpful(revdec);
            })
            .then(() => {
              routerService.getReviews(res[0].Game_id).then((re) => response.send(re));
            });
        }
      })
      .catch((error) => response.status(500).send(error));
  }
});

router.put('/game/:id/update-review', (request, response) => {
  const review = request.body;

  routerService
    .updateReview(review)
    .then(() => response.send())
    .catch((error) => response.status(500).send(error));
});

// -------------------------- LOGIN ROUTER ----------------------------------

// Sjekk token

router.post('/login', (request, response, next) => {
  const { username, password } = request?.body;

  if (username.indexOf(' ') >= 0 || password.indexOf(' ') >= 0) {
    response.send('Username or Password cant contains spaces');
    return;
  }
  if (!username.trim() || !password.trim()) {
    response.send('Insert Username and Password');
    return;
  }

  /*@ts-ignore */
  passport.authenticate('local', (err, user, info) => {
    if (err) response.send(err);
    else {
      /*@ts-ignore */
      request.logIn(user, (err) => {
        if (err) throw err;
        response.send('Successfully Authenticated');
      });
    }
  })(request, response, next);
});
router.post('/register', (request, response) => {
  const { username, password } = request?.body;

  if (request.body.username.indexOf(' ') >= 0 || request.body.username.indexOf(' ') >= 0) {
    response.send('Username or Password cant contains spaces');
    return;
  }
  if (
    !username.trim() ||
    !password.trim() ||
    typeof username !== 'string' ||
    typeof password !== 'string'
  ) {
    response.send('Imporper Values');
    return;
  } else if (username.length < 5 || password.length < 5) {
    response.send('Username and Password must have at least 5 digits');
    return;
  }
  //@ts-ignore
  if (isFinite(username)) {
    response.send('Username cant be only numbers');
    return;
  }
  LoginService.getUser(username)
    .then(async (res: any) => {
      if (res.length != 0 && res[0].username == username) {
        response.send('User Already Exists');
      } else if (res.length === 0) {
        const HashedPssword = await bcrypt.hash(password, 10);
        const user: any = {
          username: username,
          passord: HashedPssword,
        };
        await LoginService.register(user);
        response.send('User Created');
      }
    })
    .catch((error) => response.send(error));
});
router.get('/user', async (request, response) => {
  /*@ts-ignore */
  response.send({ user: request.user, isAuthenticated: request.isAuthenticated() });
});
/*@ts-ignore */
var logout = function (req, res, next) {
  // Get rid of the session token. Then call `logout`; it does no harm.
  req.logout();
  /*@ts-ignore */
  req.session.destroy(function (err) {
    if (err) {
      return next(err);
    }
    // The response should indicate that the user is no longer authenticated.
    return res.send({ isAuthenticated: req.isAuthenticated() });
  });
};

router.get('/logout', logout, function (req, res) {});

////////// GAME SERVICE ////////////

// Tafi ubrukt: fbfe135edb6347888a32702628b79b77

router.get('/games/:platform/:title', (request, response) => {
  if (request.params.title != 'none' && request.params.platform == 'all') {
    var options: any = {
      method: 'GET',
      url: `https://rawg-video-games-database.p.rapidapi.com/games?search=${request.params.title}&key=01920fbc5f92402b99ef3e8745c3ac6a`,
      headers: {
        'x-rapidapi-host': 'rawg-video-games-database.p.rapidapi.com',
        'x-rapidapi-key': '01ca4c338bmsha889479063c87dfp1ede8cjsnc90bc58e62ec',
      },
    };
  } else if (request.params.platform != 'all' && request.params.title != 'none') {
    var options: any = {
      method: 'GET',
      url: `https://rawg-video-games-database.p.rapidapi.com/games?platforms=${request.params.platform}&search=${request.params.title}&key=01920fbc5f92402b99ef3e8745c3ac6a`,
      headers: {
        'x-rapidapi-host': 'rawg-video-games-database.p.rapidapi.com',
        'x-rapidapi-key': '01ca4c338bmsha889479063c87dfp1ede8cjsnc90bc58e62ec',
      },
    };
  } else if (request.params.platform != 'all' && request.params.title == 'none') {
    var options: any = {
      method: 'GET',
      url: `https://rawg-video-games-database.p.rapidapi.com/games?platforms=${request.params.platform}&key=01920fbc5f92402b99ef3e8745c3ac6a`,
      headers: {
        'x-rapidapi-host': 'rawg-video-games-database.p.rapidapi.com',
        'x-rapidapi-key': '01ca4c338bmsha889479063c87dfp1ede8cjsnc90bc58e62ec',
      },
    };
  } else {
    var options: any = {
      method: 'GET',
      url: `https://rawg-video-games-database.p.rapidapi.com/games?key=01920fbc5f92402b99ef3e8745c3ac6a`,
      headers: {
        'x-rapidapi-host': 'rawg-video-games-database.p.rapidapi.com',
        'x-rapidapi-key': '01ca4c338bmsha889479063c87dfp1ede8cjsnc90bc58e62ec',
      },
    };
  }
  return axios
    .request(options)
    .then(function (res) {
      if (res.status == 200) {
        response.send(res.data);
      } else {
        throw Error('error fetching data');
      }
    })
    .catch(function (error) {
      console.error(error);
    });
});

router.get('/game/details/:id', (request, response) => {
  const options: any = {
    method: 'GET',
    url: `https://rawg-video-games-database.p.rapidapi.com/games/${request.params.id}?key=01920fbc5f92402b99ef3e8745c3ac6a`,
    headers: {
      'x-rapidapi-host': 'rawg-video-games-database.p.rapidapi.com',
      'x-rapidapi-key': '01ca4c338bmsha889479063c87dfp1ede8cjsnc90bc58e62ec',
    },
  };

  return axios
    .request(options)
    .then(function (res) {
      response.send(res.data);
    })
    .catch(function (error) {
      console.error(error);
    });
});

export default router;
