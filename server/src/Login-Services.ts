import pool from './mysql-pool';

export type User = {
  id: number;
  username: number;
  passord: number;
};

class LoginServices {
  getUsers() {
    return new Promise<User>((resolve, reject) => {
      pool.query('SELECT * FROM users', [], (error, results) => {
        if (error) return reject(error);
        resolve(results);
      });
    });
  }
  getUsersId(id: number) {
    return new Promise<User>((resolve, reject) => {
      pool.query('SELECT * FROM users WHERE id = ?', [id], (error, results) => {
        if (error) return reject(error);

        resolve(results[0]);
      });
    });
  }

  getUser(username: string) {
    return new Promise<User>((resolve, reject) => {
      pool.query(
        'SELECT * FROM users WHERE username IN ("", (SELECT username FROM users WHERE username = ?))',
        [username],
        (error, results) => {
          if (error) return reject(error);

          resolve(results);
        }
      );
    });
  }
  register(user: User) {
    return new Promise<number>((resolve, reject) => {
      pool.query(
        'INSERT INTO users (username, passord) VALUES (?, ?)',
        [user.username, user.passord],
        (error, results) => {
          if (error) return reject(error);
          resolve(Number(results.insertId));
        }
      );
    });
  }
}

const LoginService = new LoginServices();
export default LoginService;
