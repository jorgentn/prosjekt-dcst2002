import LoginService from './Login-Services';
//@ts-ignore
import bcrypt from 'bcryptjs';
const LocalStrategy = require('passport-local').Strategy;
//@ts-ignore
module.exports = function (passport) {
  passport.use(
    //@ts-ignore
    new LocalStrategy(function (username, password, done) {
      LoginService.getUser(username).then((res: any) => {
        if (res.length === 0) {
          return done('Incorrect username', false);
        }
        // @ts-ignore
        bcrypt.compare(password, res[0].passord, (err: string, results: any) => {
          if (err) {
            return done(err);
          }
          if (results === true) {
            return done(null, res[0]);
          } else {
            return done('Incorrect Password', false);
          }
        });
      });
    })
  );
  //@ts-ignore
  passport.serializeUser((user, done) => {
    done(null, user.id);
  });
  //@ts-ignore
  passport.deserializeUser((id, done) => {
    LoginService.getUsersId(id).then((res) => {
      const userInformation = {
        id: res.id,
        username: res.username,
      };
      done(null, userInformation);
    });
  });
};
