import app from './app';
import express from 'express';
import path from 'path';
import WhiteboardServer from './Review-server';
import http from 'http';

// Serve client files
app.use(express.static(path.join(__dirname, '/../../client/public')));

const webServer = http.createServer(app);
new WhiteboardServer(webServer, '/api/v1');

const port = 3000;
webServer.listen(port, () => {
  console.info(`Server running on port ${port}`);
});

// new WhiteboardServer(app, '/ffs');

// const port = 3000;
// app.listen(port, () => {
//   console.info(`Server running on port ${port}`);
// });
