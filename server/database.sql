CREATE TABLE review (
   review_id INT PRIMARY KEY AUTO_INCREMENT,
   id INT,
   title Varchar(32),
   review VARCHAR(1024),
   rating INT,
   helpful INT,
   not_helpful INT,
   user_name VARCHAR(64)
  
);


INSERT INTO review (id, title, review, rating, helpful, not_helpful, user_name) VALUES (3498, "Good", "Blabla", 4, 20, 3, "ffs");
INSERT INTO review (id, title, review, rating, helpful, not_helpful, user_name) VALUES (3498, "Bad", "Basaasdlalalalallablbablalblballba", 1, 5, 30, null);


CREATE TABLE rating (
   id INT PRIMARY KEY,
   rating_total INT,      
   rating_count INT            
);



INSERT INTO rating (id, rating_total, rating_count) VALUES (3498, 20, 4);



CREATE TABLE users (
id INT PRIMARY KEY AUTO_INCREMENT,
username varchar(32) NOT NULL,
passord varchar(64) NOT NULL
);



create TABLE user_like (
   id INT PRIMARY KEY AUTO_INCREMENT,
   user_id INT NOT NULL,
   review_id INT NOT NULL, 
   helpful boolean, 
   not_helpful boolean 
   FOREIGN KEY (review_id)
        REFERENCES review(review_id)
        ON DELETE CASCADE
);


