import axios from 'axios';
import app from '../src/app';
import http from 'http';
import routerService from '../src/router-service';
import LoginService from '../src/Login-Services';

axios.defaults.baseURL = 'http://localhost:3001/';

let webServer: any;
beforeAll((done) => {
  webServer = http.createServer(app);
  // Use separate port for testing
  webServer.listen(3001, () => done());
});

afterAll((done) => {
  if (!webServer) return done.fail(new Error());
  webServer.close(() => done());
});

let testDataRatings = [
  { id: 1, rating_total: 100, rating_count: 20 },
  { id: 2, rating_total: 200, rating_count: 30 },
  { id: 3, rating_total: 6, rating_count: 6 },
];

let testReview = {
  review_id: 1,
  id: 1,
  title: 'Bad',
  review: 'blabla',
  rating: 1,
  helpful: 4,
  not_helpful: 10,
  user_name: 'TestUser1',
  state: false,
};

let testResponse = {
  rating: [{ id: 1, rating_total: 100, rating_count: 20 }],
  review: [
    {
      review_id: 1,
      id: 1,
      title: 'Bad',
      review: 'blabla',
      rating: 1,
      helpful: 5,
      not_helpful: 10,
      user_name: 'TestUser1',
      state: false,
    },
  ],
};

let Review_Userlike = [
  {
    helpful: false,
    not_helpful: false,
    Game_id: 3328,
    TotalHelpful: 2,
    TotalNotHelpful: 0,
    ReviewId: 68,
  },
  {
    helpful: true,
    not_helpful: true,
    Game_id: 3328,
    TotalHelpful: 2,
    TotalNotHelpful: 0,
    ReviewId: 68,
  },
];
let testUser = {
  id: '1',
  username: 'Test',
  passord: 'Test',
};

jest.mock('../src/Login-Services');

jest.mock('../src/router-service');

describe('Tests', () => {
  describe('Router-Service', () => {
    test('getAllRatings returns correctly', async () => {
      routerService.getAllRatings = jest.fn(() =>
        Promise.resolve([
          { id: 1, rating_total: 100, rating_count: 20 },
          { id: 2, rating_total: 200, rating_count: 30 },
          { id: 3, rating_total: 6, rating_count: 6 },
        ])
      );
      const response = await axios.get('/rating');

      expect(response.status).toEqual(200);

      expect(response.data).toEqual(testDataRatings);
    });
    test('getAllRatings returns (500 Internal Server error)', async () => {
      routerService.getAllRatings = jest.fn(() => Promise.reject(new Error()));
      await axios.get('/rating').catch((error) => {
        expect(error.message).toEqual('Request failed with status code 500');
        expect(error.response.status).toEqual(500);
      });
    });

    test('createReview returns correctly', async () => {
      routerService.createReview = jest.fn(() => Promise.resolve());

      await axios
        .post('/game/1/new-review', {
          id: 1,
          review: 'asdad',
          title: 'asdad',
          rating: 4,
          user_name: 'Testuser',
        })
        .then((response) => {
          expect(response.statusText).toEqual('OK');
          expect(response.status).toEqual(200);
        });
    });

    test('createReview returns(500 Internal Server error)', async () => {
      routerService.createReview = jest.fn(() => Promise.reject());

      await axios
        .post('/game/1/new-review', {
          id: 1,
          review: 'asdad',
          title: 'asdad',
          rating: 4,
          user_name: 'Testuser',
        })
        .catch((error) => {
          expect(error.message).toEqual('Request failed with status code 500');
          expect(error.response.status).toEqual(500);
        });
    });

    test('test topReview return correctly ', async () => {
      routerService.getTopReview = jest.fn(() => Promise.resolve(testReview));

      await axios.get('/game/1/top-review').then((response) => {
        expect(response.status).toEqual(200);
        expect(response.data).toEqual(testReview);
      });
    });

    test('test topReview return (500 Internal Server error) ', async () => {
      routerService.getTopReview = jest.fn(() => Promise.reject());

      await axios.get('/game/1/top-review').catch((error) => {
        expect(error.response.status).toEqual(500);
        expect(error.message).toEqual('Request failed with status code 500');
      });
    });
    test('test topReview return status 404 ', async () => {
      routerService.getTopReview = jest.fn(() => Promise.reject([]));

      await axios.get('/game//top-review').catch((error) => {
        expect(error.response.status).toEqual(404);

        expect(error.message).toEqual('Request failed with status code 404');
      });
    });

    test('getRating and getReviews returns correctly', async () => {
      routerService.getRating = jest.fn(() => Promise.resolve(testResponse.rating));
      routerService.getReviews = jest.fn(() => Promise.resolve(testResponse.review));

      const response = await axios.get('/game/1');

      expect(response.status).toEqual(200);

      expect(response.data).toEqual(testResponse);
    });

    test('getRating and getReviews (500 Internal Server error)', async () => {
      routerService.getRating = jest.fn(() => Promise.reject());
      routerService.getReviews = jest.fn(() => Promise.reject());

      await axios.get('/game/2').catch((error) => {
        expect(error.response.status).toEqual(500);
      });
    });

    test('like_user route returns correctly', async () => {
      //@ts-ignore
      routerService.getLike_User = jest.fn(() =>
        Promise.resolve({
          id: 1,
          user_id: 1,
          review_id: 1,
          helpful: 1,
          not_helpful: 0,
        })
      );

      axios.get('/like_user/1').then((res) => {
        expect(res.status).toEqual(200);
      });
    });

    test('delete Review returns correctly', async () => {
      routerService.deleteReview = jest.fn(() => Promise.resolve());
      const response = await axios.delete('/game/1/delete-review');

      expect(response.status).toEqual(200);
    });
    test('delete Review returns (500 Internal Server error)', async () => {
      routerService.deleteReview = jest.fn(() => Promise.reject());
      await axios
        .delete('/game/1/delete-review')
        .catch((error) => expect(error.response.status).toEqual(500));
    });

    test('Create Rating returns correctly', async () => {
      routerService.createRating = jest.fn(() => Promise.resolve(3));

      const response = await axios.post('/game/3');

      expect(response.status).toEqual(200);
    });

    test('Create Rating returns (500 Internal Server error)', async () => {
      routerService.createRating = jest.fn(() => Promise.reject());

      await axios.post('/game/3').catch((error) => expect(error.response.status).toEqual(500));
    });

    test('Update Rating returns correctly', async () => {
      routerService.getRating = jest.fn(() =>
        Promise.resolve([
          {
            id: 1,
            rating_total: 100,
            rating_count: 20,
          },
        ])
      );
      routerService.updateRating = jest.fn(() => Promise.resolve());

      const response = await axios.put('/game/3', { id: 3, rating: 6 });

      expect(response.status).toEqual(200);
    });

    test('Update Rating returns status 400 (Missing data)', async () => {
      routerService.getRating = jest.fn(() =>
        Promise.resolve([
          {
            id: 1,
            rating_total: 100,
            rating_count: 20,
          },
        ])
      );
      routerService.updateRating = jest.fn(() => Promise.resolve());

      await axios.put('/game/3', {}).catch((error) => {
        expect(error.response.status).toEqual(400);

        expect(error.response.data).toEqual('Missing data');
      });
    });

    test('Update Rating returns status (500 Internal Server error) ', async () => {
      routerService.getRating = jest.fn(() =>
        Promise.resolve([
          {
            id: 1,
            rating_total: 100,
            rating_count: 20,
          },
        ])
      );
      routerService.updateRating = jest.fn(() => Promise.reject());

      await axios.put('/game/3', { id: 3, rating: 6 }).catch((error) => {
        expect(error.response.status).toEqual(500);
      });
    });

    test('updateHelpful returns correctly (status none)', async () => {
      routerService.CreateLike_User = jest.fn(() => Promise.resolve());
      // @ts-ignore
      routerService.getReview = jest.fn(() => Promise.resolve([testReview]));
      routerService.getReviews = jest.fn(() => Promise.resolve(testResponse.review));
      routerService.updateHelpful = jest.fn(() => Promise.resolve());

      const response = await axios.put('/game/3/helpful', {
        user_id: 17,
        helpful: true,
        nothelpful: false,
        status: 'None',
      });

      expect(response.status).toEqual(200);
    });

    test('updateHelpful returns correctly (status Found, helful = true)', async () => {
      // @ts-ignore
      // routerService.getReview = jest.fn(() => Promise.resolve([testReview]));
      routerService.UpdateLike_User = jest.fn(() => Promise.resolve());
      routerService.getReviews = jest.fn(() => Promise.resolve(testResponse.review));
      routerService.updateHelpful = jest.fn(() => Promise.resolve());
      //@ts-ignore
      routerService.Review_Userlike = jest.fn(() => Promise.resolve([Review_Userlike[0]]));

      const response = await axios.put('/game/3/helpful', {
        user_id: 17,
        helpful: true,
        nothelpful: false,
        status: 'Found',
      });

      expect(response.status).toEqual(200);
    });

    test('updateHelpful returns correctly  (status Found, helpful = true)', async () => {
      // @ts-ignore
      routerService.UpdateLike_User = jest.fn(() => Promise.resolve());
      routerService.getReviews = jest.fn(() => Promise.resolve(testResponse.review));
      routerService.updateHelpful = jest.fn(() => Promise.resolve());
      //@ts-ignore
      routerService.Review_Userlike = jest.fn(() => Promise.resolve([Review_Userlike[1]]));

      const response = await axios.put('/game/3/helpful', {
        user_id: 17,
        helpful: true,
        nothelpful: false,
        status: 'Found',
      });

      expect(response.status).toEqual(200);
    });

    test('updateHelpful returns correctly (status Found, helful = false)', async () => {
      routerService.UpdateLike_User = jest.fn(() => Promise.resolve());
      routerService.getReviews = jest.fn(() => Promise.resolve(testResponse.review));
      routerService.updateHelpful = jest.fn(() => Promise.resolve());
      //@ts-ignore
      routerService.Review_Userlike = jest.fn(() => Promise.resolve([Review_Userlike[0]]));

      const response = await axios.put('/game/3/helpful', {
        user_id: 17,
        helpful: false,
        nothelpful: false,
        status: 'Found',
      });

      expect(response.status).toEqual(200);
    });

    test('updateHelpful returns (500 Internal Server error) ', async () => {
      routerService.Review_Userlike = jest.fn(() => Promise.reject());

      await axios
        .put('/game/3/helpful', {
          user_id: 17,
          helpful: false,
          nothelpful: false,
          status: 'Found',
        })
        .catch((error) => expect(error.response.status).toEqual(500));
    });

    test('updateNotHelpful returns correctly (status none)', async () => {
      routerService.CreateLike_User = jest.fn(() => Promise.resolve());
      // @ts-ignore
      routerService.getReview = jest.fn(() => Promise.resolve([testReview]));
      routerService.getReviews = jest.fn(() => Promise.resolve(testResponse.review));
      routerService.updateHelpful = jest.fn(() => Promise.resolve());

      const response = await axios.put('/game/3/nothelpful', {
        user_id: 17,
        helpful: true,
        nothelpful: false,
        status: 'None',
      });

      expect(response.status).toEqual(200);
    });

    test('updateNotHelpful returns correctly (status Found, nothelful = true)', async () => {
      // @ts-ignore
      // routerService.getReview = jest.fn(() => Promise.resolve([testReview]));
      routerService.UpdateLike_User = jest.fn(() => Promise.resolve());
      routerService.getReviews = jest.fn(() => Promise.resolve(testResponse.review));
      routerService.updateHelpful = jest.fn(() => Promise.resolve());
      //@ts-ignore
      routerService.Review_Userlike = jest.fn(() => Promise.resolve([Review_Userlike[0]]));

      const response = await axios.put('/game/3/nothelpful', {
        user_id: 17,
        helpful: true,
        nothelpful: true,
        status: 'Found',
      });

      expect(response.status).toEqual(200);
    });

    test('updateNotHelpful returns correctly (status Found, nothelful = true)', async () => {
      // @ts-ignore
      // routerService.getReview = jest.fn(() => Promise.resolve([testReview]));
      routerService.UpdateLike_User = jest.fn(() => Promise.resolve());
      routerService.getReviews = jest.fn(() => Promise.resolve(testResponse.review));
      routerService.updateHelpful = jest.fn(() => Promise.resolve());
      //@ts-ignore
      routerService.Review_Userlike = jest.fn(() => Promise.resolve([Review_Userlike[1]]));

      const response = await axios.put('/game/3/nothelpful', {
        user_id: 17,
        helpful: true,
        nothelpful: true,
        status: 'Found',
      });

      expect(response.status).toEqual(200);
    });

    test('updateNotHelpful returns correctly (status Found, nothelful = false)', async () => {
      // @ts-ignore
      // routerService.getReview = jest.fn(() => Promise.resolve([testReview]));
      routerService.UpdateLike_User = jest.fn(() => Promise.resolve());
      routerService.getReviews = jest.fn(() => Promise.resolve(testResponse.review));
      routerService.updateHelpful = jest.fn(() => Promise.resolve());
      //@ts-ignore
      routerService.Review_Userlike = jest.fn(() => Promise.resolve([Review_Userlike[0]]));

      const response = await axios.put('/game/3/nothelpful', {
        user_id: 17,
        helpful: true,
        nothelpful: false,
        status: 'Found',
      });

      expect(response.status).toEqual(200);
    });

    test('updateNotHelpful returns (500 Internal Server error) ', async () => {
      routerService.Review_Userlike = jest.fn(() => Promise.reject());

      await axios
        .put('/game/3/nothelpful', {
          user_id: 17,
          helpful: false,
          nothelpful: false,
          status: 'Found',
        })
        .catch((error) => expect(error.response.status).toEqual(500));
    });

    test('updateReview returns correctly', async () => {
      routerService.updateReview = jest.fn(() => Promise.resolve());
      const response = await axios.put('/game/3/update-review', {
        title: 'Test',
        review: 'Test',
        review_id: 3,
      });
      expect(response.status).toEqual(200);
    });
    test('updateReview returns (500 Internal Server error)', async () => {
      routerService.updateReview = jest.fn(() => Promise.reject());
      await axios
        .put('/game/3/update-review', {
          title: 'Test',
          review: 'Test',
          review_id: 3,
        })
        .catch((error) => expect(error.response.status).toEqual(500));
    });
  });

  /////////////////////////////
  //// Login-service /////////
  ///////////////////////////

  describe('Login-service', () => {
    test('login returns correctly when username / password contains spaces', async () => {
      const response = await axios.post('/login', {
        username: 'None xistant user',
        passord: 'asadsdg 11231',
      });
      expect(response.data).toEqual('Username or Password cant contains spaces');
      expect(response.status).toEqual(200);
    });

    test.skip('login returns correctly when username / password are empty', async () => {
      //@ts-ignore
      LoginService.getUser = jest.fn(() =>
        Promise.resolve([
          {
            id: 17,
            username: 'ffs',
            passord: '$2a$10$fzUetOe1SCJTDpT63KhS1eTGM4EHy81Wq5e35weIhmnxQVc9ipfi6',
          },
        ])
      );
      //@ts-ignore
      LoginService.getUsersId = jest.fn(() =>
        Promise.resolve({
          id: 17,
          username: 'ffs',
          passord: '$2a$10$fzUetOe1SCJTDpT63KhS1eTGM4EHy81Wq5e35weIhmnxQVc9ipfi6',
        })
      );

      await axios
        .post('/login', {
          username: 'ffsff',
          passord: 'ffs',
        })
        .catch();
    });

    test('register Works Correctly', async () => {
      //@ts-ignore
      LoginService.getUser = jest.fn(() => Promise.resolve([]));
      //@ts-ignore
      LoginService.register = jest.fn(() => Promise.resolve());

      const response = await axios.post('/register', {
        username: 'Testaaaa1',
        password: 'Tesasdsgdsfsdf',
      });

      expect(response.status).toEqual(200);
      expect(response.data).toEqual('User Created');
    });

    test('register returns User already exists', async () => {
      //@ts-ignore
      LoginService.getUser = jest.fn(() => Promise.resolve([{ username: 'Jørgen' }]));

      const response = await axios.post('/register', {
        username: 'Jørgen',
        password: '123456',
      });

      expect(response.status).toEqual(200);
      expect(response.data).toEqual('User Already Exists');
    });

    test('register returns correctly when username / password contains spaces', async () => {
      const response = await axios.post('/register', {
        username: 'Testa aaa1',
        password: 'Tesasd sgdsfsdf',
      });

      expect(response.status).toEqual(200);
      expect(response.data).toEqual('Username or Password cant contains spaces');
    });
    test('register returns correctly when username / password are improper values', async () => {
      const response = await axios.post('/register', {
        username: '',
        password: '',
      });

      expect(response.status).toEqual(200);
      expect(response.data).toEqual('Imporper Values');
    });

    test('register returns correctly when username  contains only numbers', async () => {
      const response = await axios.post('/register', {
        username: '23242342',
        password: '23432424',
      });

      expect(response.status).toEqual(200);
      expect(response.data).toEqual('Username cant be only numbers');
    });

    test('register returns correctly when username / password must contain at least 5 digits', async () => {
      const response = await axios.post('/register', {
        username: 'sd',
        password: 'sds',
      });

      expect(response.status).toEqual(200);
      expect(response.data).toEqual('Username and Password must have at least 5 digits');
    });

    test('register returns (500 Internal Server error)', async () => {
      LoginService.getUser = jest.fn(() => Promise.reject());

      await axios
        .post('/register', {
          username: 'sd',
          password: 'sds',
        })
        .catch((error) => expect(error.response.status).toEqual(500));
    });

    test('logout', async () => {
      const response = await axios.get('/logout');
      expect(response.status).toEqual(200);
    });

    test('Get users returns correctly', async () => {
      const response = await axios.get('/user');

      expect(response.status).toEqual(200);
    });
  });

  //   /////////////////////////////
  //   //// Game-service /////////
  //   ///////////////////////////

  describe('Game-service', () => {
    test('Search for platform and title', async () => {
      const response = await axios.get('/games/pc/mario');
      expect(response.status).toEqual(200);
    });

    test('Search for all platforms and title', async () => {
      const response = await axios.get('/games/all/mario');
      expect(response.status).toEqual(200);
    });

    test('Search for all platforms and titles', async () => {
      const response = await axios.get('/games/all/none');
      expect(response.status).toEqual(200);
    });

    test('Search for all titles on pc', async () => {
      const response = await axios.get('/games/pc/none');
      expect(response.status).toEqual(200);
    });

    test('Get details for a game', async () => {
      const response = await axios.get('/game/details/3');

      expect(response.status).toEqual(200);
      expect(response.data.id).toEqual(3);
    });
  });
});
