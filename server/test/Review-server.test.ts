import http from 'http';
import WebSocket from 'ws';
import ReviewServer from '../src/Review-server';

let webServer: any;
beforeAll((done) => {
  webServer = http.createServer();
  new ReviewServer(webServer, '/api/v1');
  // Use separate port for testing
  webServer.listen(3002, () => done());
});

afterAll((done) => {
  if (!webServer) return done.fail(new Error());
  webServer.close(() => done());
});

describe('WhiteboardServer tests', () => {
  test('Connection opens successfully', (done) => {
    const connection = new WebSocket('ws://localhost:3002/api/v1/review');

    connection.on('open', () => {
      connection.close();
      done();
    });

    connection.on('error', (error) => {
      done.fail(error);
    });
  });

  test('WhiteboardServer replies correctly', (done) => {
    const connection = new WebSocket('ws://localhost:3002/api/v1/review');

    connection.on('open', () => connection.send('test'));

    connection.on('message', (data) => {
      expect(data.toString()).toEqual('test');
      connection.close();
      done();
    });

    connection.on('error', (error) => {
      done.fail(error);
    });
  });
});
