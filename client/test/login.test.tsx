import * as React from 'react';
import { shallow } from 'enzyme';
import { Register } from '../src/login';

/* Funker ikke med hooks, lar det ligge */

jest.mock('../src/LoginService', () => {
  class LoginService {
    register = () => {
      return Promise.resolve({ data: 'User Created' });
    };
  }
  return new LoginService();
});

describe('Register test', () => {
  test('Test', (done) => {
    const wrapper = shallow(<Register />);

    setTimeout(() => {
      wrapper
        .find('input')
        .first()
        .simulate('change', { target: { value: 'wqetrte' } });
      wrapper
        .find('input')
        .last()
        .simulate('change', { target: { value: 'qwetrafd' } });

      expect(wrapper.containsMatchingElement(<input />)).toEqual(true);

      wrapper.find('button').simulate('click');

      done();
    });
  });
});
