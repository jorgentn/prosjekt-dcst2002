import * as React from 'react';
import { shallow } from 'enzyme';
import NavBar from '../src/navbar';
import Home from '../src/home';

jest.mock('../src/game-service', () => {
  class GameService {
    get() {
      return Promise.resolve({
        id: 3498,
        name: 'Grand Theft Auto V',
        discription: 'Bla Bla...',
      });
    }

    getAll() {
      return Promise.resolve([
        { id: 3498, name: 'Grand Theft Auto V', discription: 'Bla Bla...' },
        { id: 2, title: 'Møt opp på forelesning', discription: 'Bla Bla...' },
        { id: 3, title: 'Gjør øving', discription: 'Bla Bla...' },
      ]);
    }
  }
  return new GameService();
});

describe('Home component tests', () => {
  test('Home contains NavBar', (done) => {
    const wrapper = shallow(<Home />);
    expect(wrapper.containsAllMatchingElements([<NavBar />])).toEqual(true);
    done();
  });
});

describe('NavBar component tests', () => {
  test('NavBar draws correctly', (done) => {
    const wrapper = shallow(<NavBar />);
    setTimeout(() => {
      expect(wrapper.containsAllMatchingElements([<button>GameRating</button>])).toEqual(true);
      done();
    });
  });

  /* Could not get this to work because of hooks. Path should be "/game/3" initially and "/" after clicked */

  test('NavBar button behaves correctly', (done) => {
    var testParams = [{ match: { path: '/' } }];

    const setSearched = () => {
      console.log('Setsearched runs');
    };

    const wrapper = shallow(<NavBar props={testParams[0]} setSearched={setSearched} />);

    setTimeout(() => {
      expect(location.hash).toEqual('');
      done();
    });
  });
});
