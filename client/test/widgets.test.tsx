import React from 'react';
import { shallow } from 'enzyme';
import { Form, Button, Column, Icon, Row, Card } from '../src/widgets';
import DiceRack from '../src/dice-rack';

describe('Widget Tests', () => {
  describe('Formating widgets', () => {
    it('Card returns correctly', () => {
      const wrapper = shallow(<Card title={'Test'}>Test</Card>);

      expect(
        wrapper.containsAllMatchingElements([
          <div className="card">
            <div className="card-body">
              <h5 className="card-title">Test</h5>
              <div className="card-text">Test</div>
            </div>
          </div>,
        ])
      ).toEqual(true);
    });

    it('Row returns correctly', () => {
      const wrapper = shallow(<Row>Hello</Row>);

      expect(wrapper.containsAllMatchingElements([<div className="row">Hello</div>])).toEqual(true);
    });

    it('Column returns correctly', () => {
      const wrapper = shallow(<Column></Column>);

      expect(
        wrapper.containsAllMatchingElements([
          <div className="col">
            <div className="float-start" />
          </div>,
        ])
      ).toEqual(true);
    });
  });

  describe('Inputs', () => {
    it('Form.Input', () => {
      const wrapper = shallow(
        <Form.Input type={'text'} value={''} onChange={() => {}}></Form.Input>
      );

      expect(
        wrapper.containsAllMatchingElements([
          <input className="form-control" type="text" value="" />,
        ])
      ).toEqual(true);

      wrapper.setProps({ value: 'ChangedValue' });

      expect(
        wrapper.containsAllMatchingElements([
          <input className="form-control" type="text" value="ChangedValue" />,
        ])
      ).toEqual(true);
    });

    it('Form.TextArea', () => {
      const wrapper = shallow(<Form.Textarea value={''} onChange={() => {}}></Form.Textarea>);

      expect(
        wrapper.containsAllMatchingElements([<textarea className="form-control" value="" />])
      ).toEqual(true);

      wrapper.setProps({ value: 'ChangedValue' });

      expect(
        wrapper.containsAllMatchingElements([
          <textarea className="form-control" value="ChangedValue" />,
        ])
      ).toEqual(true);
    });
  });

  describe('Buttons', () => {
    it('Button.Success returns correctly and onClick functions', () => {
      const wrapper = shallow(
        <Button.Success
          onClick={() => {
            result = 'Clicked';
          }}
        ></Button.Success>
      );

      var result = 'Unclicked';
      wrapper.simulate('click');

      expect(result).toEqual('Clicked');

      expect(
        wrapper.containsAllMatchingElements([<button type="button" className="btn btn-success" />])
      ).toEqual(true);
    });

    it('Button.Light returns correctly', () => {
      const wrapper = shallow(<Button.Light onClick={() => {}}></Button.Light>);

      expect(
        wrapper.containsAllMatchingElements([<button type="button" className="btn btn-light" />])
      ).toEqual(true);
    });
  });

  describe('Icons', () => {
    it('Icon.Delete', () => {
      const wrapper = shallow(
        //@ts-ignore
        <Icon.Delete />
      );

      expect(wrapper).toMatchSnapshot();
    });

    it('Icon.Edit', () => {
      const wrapper = shallow(
        //@ts-ignore
        <Icon.Edit />
      );

      expect(wrapper).toMatchSnapshot();
    });
    it('DiceRack', () => {
      const wrapper = shallow(
        //@ts-ignore
        <DiceRack />
      );

      expect(wrapper).toMatchSnapshot();
    });

    it('Icon.Dice6 returns correctly', () => {
      const wrapper = shallow(
        <Icon.Dice6
          color="red"
          onClick={() => {
            wrapper.setProps({ color: 'green' });
          }}
        ></Icon.Dice6>
      );

      expect(
        wrapper.containsAllMatchingElements([
          <svg
            color="red"
            xmlns="http://www.w3.org/2000/svg"
            width="32"
            height="32"
            fill="currentColor"
            className="bi bi-dice-6"
            viewBox="0 0 16 16"
          >
            <path d="M13 1a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h10zM3 0a3 3 0 0 0-3 3v10a3 3 0 0 0 3 3h10a3 3 0 0 0 3-3V3a3 3 0 0 0-3-3H3z" />
            <path d="M5.5 4a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm8 0a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0 8a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-4a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm-8 4a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-4a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
          </svg>,
        ])
      ).toEqual(true);

      wrapper.simulate('click');

      expect(
        wrapper.containsAllMatchingElements([
          <svg
            color="green"
            xmlns="http://www.w3.org/2000/svg"
            width="32"
            height="32"
            fill="currentColor"
            className="bi bi-dice-6"
            viewBox="0 0 16 16"
          >
            <path d="M13 1a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h10zM3 0a3 3 0 0 0-3 3v10a3 3 0 0 0 3 3h10a3 3 0 0 0 3-3V3a3 3 0 0 0-3-3H3z" />
            <path d="M5.5 4a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm8 0a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0 8a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-4a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm-8 4a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-4a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
          </svg>,
        ])
      ).toEqual(true);
    });
    it('Icon.Dice5 returns correctly', () => {
      const wrapper = shallow(
        <Icon.Dice5
          color="red"
          onClick={() => {
            wrapper.setProps({ color: 'green' });
          }}
        ></Icon.Dice5>
      );

      expect(
        wrapper.containsAllMatchingElements([
          <svg
            color="red"
            xmlns="http://www.w3.org/2000/svg"
            width="32"
            height="32"
            fill="currentColor"
            className="bi bi-dice-5"
            viewBox="0 0 16 16"
          >
            <path d="M13 1a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h10zM3 0a3 3 0 0 0-3 3v10a3 3 0 0 0 3 3h10a3 3 0 0 0 3-3V3a3 3 0 0 0-3-3H3z" />
            <path d="M5.5 4a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm8 0a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0 8a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm-8 0a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm4-4a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
          </svg>,
        ])
      ).toEqual(true);

      wrapper.simulate('click');

      expect(
        wrapper.containsAllMatchingElements([
          <svg
            color="green"
            xmlns="http://www.w3.org/2000/svg"
            width="32"
            height="32"
            fill="currentColor"
            className="bi bi-dice-5"
            viewBox="0 0 16 16"
          >
            <path d="M13 1a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h10zM3 0a3 3 0 0 0-3 3v10a3 3 0 0 0 3 3h10a3 3 0 0 0 3-3V3a3 3 0 0 0-3-3H3z" />
            <path d="M5.5 4a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm8 0a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0 8a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm-8 0a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm4-4a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
          </svg>,
        ])
      ).toEqual(true);
    });

    it('Icon.Dice4 returns correctly', () => {
      const wrapper = shallow(
        <Icon.Dice4
          color="red"
          onClick={() => {
            wrapper.setProps({ color: 'green' });
          }}
        ></Icon.Dice4>
      );

      expect(
        wrapper.containsAllMatchingElements([
          <svg
            color="red"
            xmlns="http://www.w3.org/2000/svg"
            width="32"
            height="32"
            fill="currentColor"
            className="bi bi-dice-4"
            viewBox="0 0 16 16"
          >
            <path d="M13 1a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h10zM3 0a3 3 0 0 0-3 3v10a3 3 0 0 0 3 3h10a3 3 0 0 0 3-3V3a3 3 0 0 0-3-3H3z" />
            <path d="M5.5 4a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm8 0a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0 8a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm-8 0a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
          </svg>,
        ])
      ).toEqual(true);

      wrapper.simulate('click');

      expect(
        wrapper.containsAllMatchingElements([
          <svg
            color="green"
            xmlns="http://www.w3.org/2000/svg"
            width="32"
            height="32"
            fill="currentColor"
            className="bi bi-dice-4"
            viewBox="0 0 16 16"
          >
            <path d="M13 1a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h10zM3 0a3 3 0 0 0-3 3v10a3 3 0 0 0 3 3h10a3 3 0 0 0 3-3V3a3 3 0 0 0-3-3H3z" />
            <path d="M5.5 4a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm8 0a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0 8a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm-8 0a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
          </svg>,
        ])
      ).toEqual(true);
    });
    it('Icon.Dice3 returns correctly', () => {
      const wrapper = shallow(
        <Icon.Dice3
          color="red"
          onClick={() => {
            wrapper.setProps({ color: 'green' });
          }}
        ></Icon.Dice3>
      );

      expect(
        wrapper.containsAllMatchingElements([
          <svg
            color="red"
            xmlns="http://www.w3.org/2000/svg"
            width="32"
            height="32"
            fill="currentColor"
            className="bi bi-dice-3"
            viewBox="0 0 16 16"
          >
            <path d="M13 1a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h10zM3 0a3 3 0 0 0-3 3v10a3 3 0 0 0 3 3h10a3 3 0 0 0 3-3V3a3 3 0 0 0-3-3H3z" />
            <path d="M5.5 4a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm8 8a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm-4-4a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
          </svg>,
        ])
      ).toEqual(true);

      wrapper.simulate('click');

      expect(
        wrapper.containsAllMatchingElements([
          <svg
            color="green"
            xmlns="http://www.w3.org/2000/svg"
            width="32"
            height="32"
            fill="currentColor"
            className="bi bi-dice-3"
            viewBox="0 0 16 16"
          >
            <path d="M13 1a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h10zM3 0a3 3 0 0 0-3 3v10a3 3 0 0 0 3 3h10a3 3 0 0 0 3-3V3a3 3 0 0 0-3-3H3z" />
            <path d="M5.5 4a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm8 8a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm-4-4a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
          </svg>,
        ])
      ).toEqual(true);
    });
    it('Icon.Dice2 returns correctly', () => {
      const wrapper = shallow(
        <Icon.Dice2
          color="red"
          onClick={() => {
            wrapper.setProps({ color: 'green' });
          }}
        ></Icon.Dice2>
      );

      expect(
        wrapper.containsAllMatchingElements([
          <svg
            color="red"
            xmlns="http://www.w3.org/2000/svg"
            width="32"
            height="32"
            fill="currentColor"
            className="bi bi-dice-2"
            viewBox="0 0 16 16"
          >
            <path d="M13 1a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h10zM3 0a3 3 0 0 0-3 3v10a3 3 0 0 0 3 3h10a3 3 0 0 0 3-3V3a3 3 0 0 0-3-3H3z" />
            <path d="M5.5 4a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm8 8a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
          </svg>,
        ])
      ).toEqual(true);

      wrapper.simulate('click');

      expect(
        wrapper.containsAllMatchingElements([
          <svg
            color="green"
            xmlns="http://www.w3.org/2000/svg"
            width="32"
            height="32"
            fill="currentColor"
            className="bi bi-dice-2"
            viewBox="0 0 16 16"
          >
            <path d="M13 1a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h10zM3 0a3 3 0 0 0-3 3v10a3 3 0 0 0 3 3h10a3 3 0 0 0 3-3V3a3 3 0 0 0-3-3H3z" />
            <path d="M5.5 4a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm8 8a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
          </svg>,
        ])
      ).toEqual(true);
    });

    it('Icon.Dice1 returns correctly', () => {
      const wrapper = shallow(
        <Icon.Dice1
          color="red"
          onClick={() => {
            wrapper.setProps({ color: 'green' });
          }}
        ></Icon.Dice1>
      );

      expect(
        wrapper.containsAllMatchingElements([
          <svg
            color="red"
            xmlns="http://www.w3.org/2000/svg"
            width="32"
            height="32"
            fill="currentColor"
            className="bi bi-dice-1"
            viewBox="0 0 16 16"
          >
            <circle cx="8" cy="8" r="1.5" />
            <path d="M13 1a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h10zM3 0a3 3 0 0 0-3 3v10a3 3 0 0 0 3 3h10a3 3 0 0 0 3-3V3a3 3 0 0 0-3-3H3z" />
          </svg>,
        ])
      ).toEqual(true);

      wrapper.simulate('click');

      expect(
        wrapper.containsAllMatchingElements([
          <svg
            color="green"
            xmlns="http://www.w3.org/2000/svg"
            width="32"
            height="32"
            fill="currentColor"
            className="bi bi-dice-1"
            viewBox="0 0 16 16"
          >
            <circle cx="8" cy="8" r="1.5" />
            <path d="M13 1a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h10zM3 0a3 3 0 0 0-3 3v10a3 3 0 0 0 3 3h10a3 3 0 0 0 3-3V3a3 3 0 0 0-3-3H3z" />
          </svg>,
        ])
      ).toEqual(true);
    });

    it('Icon.Like returns correctly when clicked', () => {
      const wrapper = shallow(
        <Icon.Like
          liked={true}
          onClick={() => {
            wrapper.setProps({ liked: false });
          }}
        />
      );

      expect(wrapper.props().fill).toEqual('green');

      wrapper.simulate('click');

      expect(wrapper.props().fill).toEqual('currentColor');
    });

    it('Icon.DisLike returns correctly when clicked', () => {
      const wrapper = shallow(
        <Icon.DisLike
          dislike={true}
          onClick={() => {
            wrapper.setProps({ dislike: false });
          }}
        />
      );

      expect(wrapper.props().fill).toEqual('red');

      wrapper.simulate('click');

      expect(wrapper.props().fill).toEqual('currentColor');
    });
  });
});
