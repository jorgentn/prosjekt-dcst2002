import * as React from 'react';
import { shallow } from 'enzyme';
import Home from '../src/home';

/* Funker ikke med hooks, lar det ligge */

jest.mock('../src/game-service', () => {
  class GameService {
    get() {
      return Promise.resolve({
        id: 3498,
        name: 'Grand Theft Auto V',
        discription: 'Bla Bla...',
      });
    }

    getAll() {
      return Promise.resolve([
        {
          id: 3498,
          name: 'Grand Theft Auto V',
          background_image:
            'https://media.rawg.io/media/screenshots/a00/a00f9ee3030522827568b21e8dd6c487.jpg',
        },
        {
          id: 32,
          name: 'Ninja Gaiden',
          background_image:
            'https://media.rawg.io/media/screenshots/a00/a00f9ee3030522827568b21e8dd6c487.jpg',
        },
        {
          id: 3,
          name: 'League of legends',
          background_image:
            'https://media.rawg.io/media/screenshots/a00/a00f9ee3030522827568b21e8dd6c487.jpg',
        },
      ]);
    }
  }
  return new GameService();
});

jest.mock('../src/database-service', () => {
  class DatabaseService {
    getRatings() {
      return Promise.resolve([
        { id: 32, rating_total: 1, rating_count: 1 },
        { id: 2454, rating_total: 1, rating_count: 1 },
      ]);
    }
  }

  return new DatabaseService();
});

describe('Home component tests', () => {
  test('Home contains NavBar', () => {
    const wrapper = shallow(<Home />);

    // console.log(wrapper.find('Row'));
    // expect(wrapper).toMatchSnapshot();

    // const wrapper = mount(<Home />).toJSON();

    //   expect(wrapper.containsAllMatchingElements([<NavBar />])).toEqual(true);
  });
});
