import axios from 'axios';
import type { Review, Rating, User } from './types/types';

axios.defaults.baseURL = 'http://localhost:3000/';

class DatabaseService {
  get(id: number) {
    return axios.get<Review>('/game/' + id).then((response) => response.data);
  }

  getTopReview(id: number) {
    return axios.get<Review>('/game/' + id + '/top-review').then((response) => response.data);
  }

  getRatings() {
    return axios.get('/rating').then((response) => response);
  }

  createRating(id: number, rating: number) {
    return axios
      .post<Rating>('/game/' + id, { id: id, rating: rating })
      .then((response) => response);
  }

  createReview(review: Review) {
    return axios
      .post<Review>('/game/' + review.id + '/new-review', {
        id: review.id,
        review: review.review,
        title: review.title,
        rating: review.rating,
        user_name: review.user_name,
      })
      .then((response) => response.data);
  }

  deleteReview(id: number) {
    return axios
      .delete<{ id: number }>('/game/' + Number(id) + '/delete-review')
      .then((response) => response.data);
  }

  updateReview(review: Review) {
    return axios
      .put<Review>('/game/' + review.id + '/update-review', {
        id: review.id,
        review_id: review.review_id,
        title: review.title,
        review: review.review,
        rating: review.rating,
        user_name: review.user_name,
      })
      .then((response) => response.data);
  }

  updateRating(id: number, rating: number) {
    return axios
      .put<Rating>('/game/' + id, {
        id: id,
        rating: rating,
      })
      .then((response) => response);
  }

  updateHelpful(
    review_id: number,
    user: User,
    helpful: boolean,
    nothelpful: boolean,
    status: String
  ) {
    return axios
      .put<Review>('/game/' + review_id + '/helpful', {
        user_id: user,
        helpful: helpful,
        nothelpful: nothelpful,
        status: status,
      })
      .then((response) => response);
  }

  updateNotHelpful(
    review_id: number,
    user: User,
    helpful: boolean,
    nothelpful: boolean,
    status: String
  ) {
    return axios
      .put<Review>('/game/' + review_id + '/nothelpful', {
        user_id: user,
        helpful: helpful,
        nothelpful: nothelpful,
        status: status,
      })
      .then((response) => response);
  }

  get_like_user(user_id: number) {
    return axios.get('/like_user/' + user_id).then((response) => response);
  }
}

const databaseservice = new DatabaseService();
export default databaseservice;
