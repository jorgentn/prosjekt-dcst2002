import React from 'react';
import { Route, HashRouter, Redirect } from 'react-router-dom';
import { Alert } from './widgets';
import { Login, Register } from './login';
import GameDetails from './game-details';
import Home from './home';
import { useUser } from './login';

//@ts-ignore
const ProtectedRoute = ({ component: Component, ...rest }) => {
  const user = useUser();
  return (
    <Route
      {...rest}
      render={(props) => {
        //@ts-ignore
        if (!user.data?.isAuthenticated) {
          return <Component {...props} />;
          //@ts-ignore
        } else if (user.data?.isAuthenticated) {
          return (
            <Redirect
              to={{
                pathname: '/',
                state: {
                  from: props.location,
                },
              }}
            />
          );
        }
      }}
    />
  );
};

const App = () => {
  return (
    <HashRouter>
      <Alert />
      <ProtectedRoute exact path="/login" component={Login} />
      <ProtectedRoute exact path="/register" component={Register} />
      <Route exact path="/" component={Home} />
      <Route exact path="/game/:id" component={GameDetails} />
    </HashRouter>
  );
};

export default App;
