import axios from 'axios';

class GameService {
  getAll(title?: string, Platform?: string) {
    if (!title?.trim()) {
      title = 'none';
    }

    return axios
      .get('/games/' + Platform + '/' + title)
      .then((response) => response.data)
      .catch(() => console.error('Error getting games from API, key might be expired'));
  }

  get(id: number) {
    return axios
      .get('/game/details/' + id)
      .then((response) => response.data)
      .catch(() => console.error('Error getting games from API, key might be expired'));
  }
}

const gameservice = new GameService();
export default gameservice;
