import ReactDOM from 'react-dom';
import React from 'react';
import App from './app';

import { GetUser } from './login';

const root = document.getElementById('root');
if (root)
  ReactDOM.render(
    <GetUser>
      <div>
        <App />
      </div>
    </GetUser>,
    root
  );
