import React, { useState, useContext, createContext } from 'react';

const testcontext = createContext({});

export const chook = () => {
  return useContext(testcontext);
};
//@ts-ignore
export const Test = ({ children }) => {
  const [value, setvalue] = useState('');

  return <testcontext.Provider value={{ setvalue, value }}>{children}</testcontext.Provider>;
};
