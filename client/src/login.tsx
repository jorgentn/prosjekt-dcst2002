import React, { useState, useEffect, useContext } from 'react';
import { useHistory, NavLink } from 'react-router-dom';
import loginservice from './LoginService';

const userContext = React.createContext({});

export function Register() {
  const [registerUsername, setRegisterUsername] = useState('');
  const [registerPassword, setRegisterPassword] = useState('');
  const [RegisterState, setRegisterState] = useState('');
  const history = useHistory();

  const register = () => {
    loginservice.register(registerUsername, registerPassword).then((res: any) => {
      setRegisterState(res.data);
      if (res.data == 'User Created') {
        setTimeout(() => {
          history.push('/login');
        }, 1000);
      }
    });
  };

  return (
    <section className="vh-100 gradient-custom">
      <div className="container py-5 h-100">
        <div className="row d-flex justify-content-center align-items-center h-100">
          <div className="col-md-8 col-lg-6 col-xl-5">
            <div className="card bg-dark text-white">
              <div className="card-body p-5 text-center">
                <div className="md-md-5 mt-md-4 pb-5"></div>
                <p>{RegisterState}</p>
                <h1>Register</h1> <br />
                <div>
                  <input
                    className="form-control form-control-lg"
                    placeholder="username"
                    id="typeuser"
                    onChange={(e) => setRegisterUsername(e.target.value)}
                  />
                  <label className="form-label" htmlFor="typeuser"></label>
                </div>
                <div className="form-outline form-white mb-4">
                  <input
                    className="form-control form-control-lg"
                    id="typeupass"
                    type="password"
                    placeholder="password"
                    onChange={(e) => setRegisterPassword(e.target.value)}
                  />
                  <label className="form-label" htmlFor="typepass"></label>
                </div>
                <button className="btn btn-secondary btn-lg px-5 text-white" onClick={register}>
                  Submit
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export function Login() {
  const user = useContext(userContext);
  const [loginUsername, setLoginUsername] = useState('');
  const [loginPassword, setLoginPassword] = useState('');
  const [LoginState, setLoginState] = useState('');

  const history = useHistory();

  const login = () => {
    loginservice.login(loginUsername, loginPassword).then((res: any) => {
      setLoginState(res.data);
      if (res.data == 'Successfully Authenticated') {
        setTimeout(() => {
          history.push('/');
          window.location.reload();
        }, 1000);
      }
    });
  };

  return (
    <section className="vh-100 gradient-custom">
      <div className="container py-5 h-100">
        <div className="row d-flex justify-content-center align-items-center h-100">
          <div className="col-md-8 col-lg-6 col-xl-5">
            <div className="card bg-dark text-white">
              <div className="card-body p-5 text-center">
                <div className="md-md-5 mt-md-4 pb-5">
                  <p>{LoginState}</p>
                  <h1 className="fw-bold mb-2 text-uppercase">Login</h1>
                  <br />
                  <div className="form-outline form-white mb-4">
                    <input
                      id="typeuser"
                      className="form-control form-control-lg"
                      placeholder="username"
                      onChange={(e) => setLoginUsername(e.target.value)}
                    />
                    <label className="form-label" htmlFor="typeuser"></label>
                  </div>
                  <div className="form-outline form-white mb-4">
                    <input
                      id="typepass"
                      className="form-control form-control-lg"
                      type="password"
                      placeholder="password"
                      onChange={(e) => setLoginPassword(e.target.value)}
                    />
                    <label className="form-label" htmlFor="typepass"></label>
                  </div>
                  <button className="btn btn-secondary btn-lg px-5 text-white" onClick={login}>
                    Submit
                  </button>
                  <br />
                  <br />
                  <div>
                    <NavLink className="btn btn-info btn-lg px-5 text-dark" to="/register">
                      Register
                    </NavLink>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
export const useUser = () => {
  return useContext(userContext);
};

export function GetUser({ children }: any) {
  const [data, setData] = useState<{} | null>(null);

  useEffect(() => {
    loginservice.getUser().then((res) => {
      setData(res);
    });
  }, [children]);

  return <userContext.Provider value={{ data }}>{children}</userContext.Provider>;
}
