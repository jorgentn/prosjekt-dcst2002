export interface Game {
  id: number;
  name: string;
  background_image: string;
  genres: any;
  rating: number;
  ratings_count: number;
  platforms: Array<object>;
  platform: Array<object>;
  description_raw: string;
}

export interface Genre {
  name: string;
}

export type RatingReview = {
  rating: Rating[];
  review: Review[];
};

export type Review = {
  h: boolean;
  review_id: number;
  id: number;
  n: boolean;
  title: string;
  review: string;
  rating: number;
  helpful: number;
  not_helpful: number;
  user_name: any;
  state: boolean;
};

export type Rating = {
  id: number;
  rating_total: number;
  rating_count: number;
};

export type User = {
  id: number;
  username: string;
};

export type UserLike = {
  helpful: boolean;
  id: number;
  not_helpful: boolean;
  review_id: number;
  user_id: number;
};
