import React from 'react';
import { NavLink } from 'react-router-dom';
import { Card, Row } from './widgets';
import NavBar from './navbar';
import type { Rating, Game } from './types/types';

const SearchTitle = (props: any) => {
  const RATING: Rating[] = props.RATING;
  let listOfSearch = props.searched.data.map((game: Game) => {
    return (
      <div
        className="col-md-3"
        key={game.id}
        style={{ margin: 'auto', width: 'auto', marginBottom: '7rem' }}
      >
        <div className="game-box">
          <NavLink to={`/game/${game.id}`} style={{ textDecoration: 'none' }}>
            <div className="game-inhold">
              <Card title={game.name}>
                {<img src={game.background_image}></img>}

                {RATING?.map((rating: Rating) => {
                  if (rating.id == game.id) {
                    return (rating.rating_total / rating.rating_count).toFixed(2);
                  }
                })}
              </Card>
            </div>
          </NavLink>
        </div>
      </div>
    );
  });

  return (
    <div>
      <NavBar setSearched={props.setSearched} props={props.props} />

      <Row>{listOfSearch}</Row>
    </div>
  );
};

export default SearchTitle;
