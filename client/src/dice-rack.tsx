import * as React from 'react';
import { Component } from 'react-simplified';
import databaseservice from './database-service';
import { Alert, Icon } from './widgets';

class DiceRack extends Component<{
  rating: string;
  id: number;
  setRating: any;
  userRating: any;
  rated: any;
  setRated: any;
}> {
  render() {
    let rating = this.props.rating;

    return (
      <div>
        <br />
        <Icon.Dice1
          onClick={() => {
            if (rating == 'Unrated') {
              this.rate(1);
              this.setUserRating(1);
            } else {
              this.updateRating(1);
              this.setUserRating(1);
            }
          }}
        />
        <Icon.Dice2
          onClick={() => {
            if (rating == 'Unrated') {
              this.rate(2);
              this.setUserRating(2);
            } else {
              this.updateRating(2);
              this.setUserRating(2);
            }
          }}
        />
        <Icon.Dice3
          onClick={() => {
            if (rating == 'Unrated') {
              this.rate(3);
              this.setUserRating(3);
            } else {
              this.updateRating(3);
              this.setUserRating(3);
            }
          }}
        />
        <Icon.Dice4
          onClick={() => {
            if (rating == 'Unrated') {
              this.rate(4);
              this.setUserRating(4);
            } else {
              this.updateRating(4);
              this.setUserRating(4);
            }
          }}
        />
        <Icon.Dice5
          onClick={() => {
            if (rating == 'Unrated') {
              this.rate(5);
              this.setUserRating(5);
            } else {
              this.updateRating(5);
              this.setUserRating(5);
            }
          }}
        />
        <Icon.Dice6
          onClick={() => {
            if (rating == 'Unrated') {
              this.rate(6);
              this.setUserRating(6);
            } else {
              this.updateRating(6);
              this.setUserRating(6);
            }
          }}
        />
      </div>
    );
  }

  rate = (rating: any) => {
    databaseservice.createRating(this.props.id, rating).then(() => {
      databaseservice
        .get(this.props.id)
        .then((response: any) => {
          this.props.setRating(response.rating[0].rating_total / response.rating[0].rating_count);
        })
        .then(() => {
          this.props.setRated(false);
        })
        .catch(() => {
          Alert.danger('Error creating new rating');
        });
    });
  };

  updateRating = (rating: any) => {
    databaseservice.updateRating(this.props.id, rating).then(() => {
      databaseservice
        .get(this.props.id)
        .then((response: any) => {
          console.log(response);
          this.props.setRating(
            (response.rating[0].rating_total / response.rating[0].rating_count).toFixed(2)
          );
        })
        .then(() => {
          this.props.setRated(false);
        })
        .catch(() => {
          Alert.danger('Error updating rating');
        });
    });
  };
  setUserRating = (rating: number) => {
    this.props.userRating(rating);
  };
}

export default DiceRack;
