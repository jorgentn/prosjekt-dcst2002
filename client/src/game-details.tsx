import React, { useState, useEffect, useRef } from 'react';
import { Card, Button, Form, Column, Row, Icon, Alert } from './widgets';
import DiceRack from './dice-rack';
import NavBar from './navbar';
import gameservice from './game-service';
import databaseservice from './database-service';
import { useUser } from './login';
import type { Review, Game, Genre } from './types/types';

const GameDetails = (props: any) => {
  const [GAME, setGame] = useState<Game | null>();
  const [REVIEWS, setReviews] = useState<any | null>([]);
  const [RATING, setRating] = useState<any | null>(['Unrated']);
  const [DETAILS, setDetails] = useState<boolean>(true);
  const [WRITEREVIEW, setWriteReview] = useState<boolean>(true);
  const [USERRATING, setUserRating] = useState<number | null>();
  const [REVIEWTITLE, setReviewTitle] = useState<string>('');
  const [REVIEWTEXT, setReviewText] = useState<string>('');
  const [NEWREVIEW, setNewReview] = useState<any>();
  const [RATED, setRated] = useState<boolean>(true);
  const user: any = useUser();
  const [REVIEWEDBYUSER, setReviewedByUser] = useState(false);
  const [helpful_user, setHelpful_user] = useState();
  let [REVIEWINFO, setReviewInfo] = useState<any | null>([]);
  const [HIDDENHELPFUL, setHiddenHelpful] = useState<string>('Unlock');
  const [HIDDENNOTHELPFUL, setHiddenNotHelpful] = useState<string>('Unlock');
  const [SHOWREVIEWS, setShowReviews] = useState<boolean>(true);
  const [isLoading, setisLoading] = useState<boolean>(false);
  const [TOPREVIEW, setTopReview] = useState<Review>();

  const [connected, setconnected] = useState<boolean>(false);

  const connection = useRef<WebSocket | null>(null);

  const updateHelpful = (review: Review, index: number) => {
    setHiddenHelpful('Locked');

    // @ts-ignore
    const helpful = helpful_user?.filter((data) => data.review_id == review.review_id);

    if (REVIEWINFO[index].h != true) {
      REVIEWINFO[index].h = true;
      REVIEWINFO[index].n = false;
    } else {
      REVIEWINFO[index].h = false;
      REVIEWINFO[index].n = false;
    }

    if (helpful.length != 0 && helpful[0].helpful == false) {
      databaseservice
        .updateHelpful(review.review_id, user.data?.user?.id, true, false, 'Found')
        .then((res) => setReviews(res.data))
        .then(() => {
          databaseservice.get_like_user(user.data?.user?.id).then((res: any) => {
            setHelpful_user(res.data);
            setHiddenHelpful('Unlock');
          });
        });
    } else if (helpful.length == 0) {
      databaseservice
        .updateHelpful(review.review_id, user.data?.user?.id, true, false, 'None')
        .then((res) => setReviews(res.data))
        .then(() => {
          databaseservice.get_like_user(user.data?.user?.id).then((res: any) => {
            setHelpful_user(res.data);
            setHiddenHelpful('Unlock');
          });
        });
    } else {
      databaseservice
        .updateHelpful(review.review_id, user.data?.user?.id, false, false, 'Found')
        .then((res) => setReviews(res.data))
        .then(() => {
          databaseservice.get_like_user(user.data?.user?.id).then((res: any) => {
            setHelpful_user(res.data);
            setHiddenHelpful('Unlock');
          });
        });
    }
  };

  const updateNotHelpful = (review: Review, index: number) => {
    setHiddenNotHelpful('Locked');

    // @ts-ignore
    const not_helpful = helpful_user?.filter((data) => data.review_id == review.review_id);
    if (REVIEWINFO[index].n != true) {
      REVIEWINFO[index].n = true;
      REVIEWINFO[index].h = false;
    } else {
      REVIEWINFO[index].n = false;
      REVIEWINFO[index].h = false;
    }

    if (not_helpful.length != 0 && not_helpful[0].not_helpful == false) {
      databaseservice
        .updateNotHelpful(review.review_id, user.data?.user?.id, false, true, 'Found')
        .then((res) => setReviews(res.data))
        .then(() => {
          databaseservice.get_like_user(user.data?.user?.id).then((res: any) => {
            setHelpful_user(res.data);
            setHiddenNotHelpful('Unlock');
          });
        });
    } else if (not_helpful.length == 0) {
      databaseservice
        .updateNotHelpful(review.review_id, user.data?.user?.id, false, true, 'None')
        .then((res) => setReviews(res.data))
        .then(() => {
          databaseservice.get_like_user(user.data?.user?.id).then((res: any) => {
            setHelpful_user(res.data);
            setHiddenNotHelpful('Unlock');
          });
        });
    } else {
      databaseservice
        .updateNotHelpful(review.review_id, user.data?.user?.id, false, false, 'Found')
        .then((res) => setReviews(res.data))
        .then(() => {
          databaseservice.get_like_user(user.data?.user?.id).then((res: any) => {
            setHelpful_user(res.data);
            setHiddenNotHelpful('Unlock');
          });
        });
    }
  };

  const deleteReview = (id: number) => {
    databaseservice.deleteReview(id).then(() => {
      databaseservice
        .get(props.match.params.id)
        .then((response: any) => {
          setReviews(response.review);
        })
        .catch(() => {
          Alert.danger('Error updating not helpful');
        });
    });
  };

  useEffect(() => {
    databaseservice.getTopReview(props.match.params.id).then((res) => setTopReview(res));
  }, [REVIEWS]);

  useEffect(() => {
    let r: Review[] = REVIEWINFO;

    REVIEWINFO.map((info: Review, index: number) => {
      //@ts-ignore

      helpful_user?.filter((data) => {
        if (data.review_id == info.review_id) {
          r[index].h = data.helpful;
          r[index].n = data.not_helpful;
        }
      });
      setReviewInfo(r);
    });

    setReviewInfo(r);
  }, [helpful_user, user]);

  useEffect(() => {
    setisLoading(true);
    databaseservice
      .get(props.match.params.id)
      .then((response: any) => {
        setReviews(response.review);
        setReviewInfo(response.review);
        setRating((response.rating[0].rating_total / response.rating[0].rating_count).toFixed(2));
      })

      .catch(() => {
        setisLoading(false);
      });

    gameservice.get(props.match.params.id).then((response) => {
      setGame(response);
    });

    databaseservice
      .get_like_user(user.data?.user?.id)
      .then((res: any) => {
        setHelpful_user(res.data);
      })
      .catch();
    setisLoading(false);
  }, [user.data?.isAuthenticated]);

  useEffect(() => {
    const revByUser = () =>
      REVIEWS.map((review: Review) => {
        if (review.user_name == user.data?.user?.username) {
          setReviewedByUser(true);
        }
      });

    revByUser();

    //mapReviews();
  }, [REVIEWS]);

  useEffect(() => {
    connection.current = new WebSocket('ws://localhost:3000/api/v1/review');

    connection.current.onopen = function (event) {
      setconnected(true);
    };

    connection.current.onerror = () => {
      setconnected(false);
      Alert.danger('Connection error');
    };

    connection.current.onmessage = (message) => {
      if (message.data == 'Review Updated') {
        databaseservice.get(props.match.params.id).then((response: any) => {
          setReviews(response.review);
          setReviewInfo(response.review);
          setRating((response.rating[0].rating_total / response.rating[0].rating_count).toFixed(2));
        });
      }
    };
  }, []);

  if (isLoading) {
    return (
      <div>
        <div className="loading-text">
          <p style={{ whiteSpace: 'nowrap' }}>Loading game details:...</p>
        </div>
        <div className="loading">
          <div className="spinner">
            <div className="mask">
              <div className="maskedCircle"></div>
            </div>
          </div>
        </div>
      </div>
    );
  } else {
    return (
      <>
        <div className="game-details">
          <NavBar props={props} />
          <Card title={GAME?.name}>
            {
              <img
                className={'.col-xs-2 .col-sm-2 .col-md-3 img-thumbnail'}
                src={GAME?.background_image}
              ></img>
            }
            {<br />}
            {<br />}
            {`Total rating: ${RATING}`}

            {
              <div hidden={RATED}>
                {USERRATING == 1 ? (
                  <Icon.Dice1 color={'green'} />
                ) : USERRATING == 2 ? (
                  <Icon.Dice2 color={'green'} />
                ) : USERRATING == 3 ? (
                  <Icon.Dice3 color={'green'} />
                ) : USERRATING == 4 ? (
                  <Icon.Dice4 color={'green'} />
                ) : USERRATING == 5 ? (
                  <Icon.Dice5 color={'green'} />
                ) : USERRATING == 6 ? (
                  <Icon.Dice6 color={'green'} />
                ) : (
                  'Unrated'
                )}{' '}
                <div hidden={REVIEWEDBYUSER}>
                  <Button.Success
                    small
                    onClick={() => {
                      WRITEREVIEW == true ? setWriteReview(false) : setWriteReview(true);
                    }}
                  >
                    Write review
                  </Button.Success>
                </div>
              </div>
            }

            {
              <div hidden={!user.data?.user}>
                <div hidden={REVIEWEDBYUSER}>
                  <div hidden={!RATED}>
                    <DiceRack
                      rating={RATING}
                      id={props.match.params.id}
                      setRating={setRating}
                      userRating={setUserRating}
                      rated={USERRATING}
                      setRated={setRated}
                    />
                  </div>
                </div>
              </div>
            }
            {
              <div hidden={WRITEREVIEW}>
                <div hidden={REVIEWEDBYUSER}>
                  Title:
                  <Form.Input
                    type={'text'}
                    value={REVIEWTITLE}
                    onChange={(event) => {
                      setReviewTitle(event.currentTarget.value);
                      setNewReview({
                        id: props.match.params.id,
                        review: REVIEWTEXT,
                        title: event.currentTarget.value,
                        rating: USERRATING,
                        //@ts-ignore
                        user_name: user.data?.user?.username,
                      });
                    }}
                  ></Form.Input>
                </div>
              </div>
            }
            {
              <div hidden={WRITEREVIEW}>
                <div hidden={REVIEWEDBYUSER}>
                  Review:
                  <Form.Textarea
                    value={REVIEWTEXT}
                    onChange={(event) => {
                      setReviewText(event.currentTarget.value);
                      setNewReview({
                        id: props.match.params.id,
                        review: event.currentTarget.value,
                        title: REVIEWTITLE,
                        rating: USERRATING,
                        //@ts-ignore
                        user_name: user.data?.user?.username,
                      });
                    }}
                  ></Form.Textarea>
                </div>
              </div>
            }
            <br />
            {
              <div hidden={WRITEREVIEW}>
                <div hidden={REVIEWEDBYUSER}>
                  <Button.Success
                    onClick={() => {
                      databaseservice.createReview(NEWREVIEW).then(() => {
                        connected == true ? connection.current?.send('Review Updated') : null;
                        databaseservice
                          .get(props.match.params.id)
                          .then((response: any) => {
                            setReviews(response.review);
                            setReviewInfo(response.review);
                            setReviewedByUser(true);
                          })
                          .catch(() => {
                            Alert.danger('Error creating new review');
                          });
                      });
                    }}
                  >
                    Submit
                  </Button.Success>
                </div>
              </div>
            }

            {<br />}
            {<br />}
            {
              <button
                className="btn btn-outline-primary btn-md px-3 text-dark"
                onClick={() => {
                  DETAILS == true ? setDetails(false) : setDetails(true);
                }}
              >
                {DETAILS == true ? 'Show details' : 'Hide details'}
              </button>
            }
            {
              <div hidden={DETAILS}>
                {<b>Genres: </b>}
                {GAME?.genres.map((genre: Genre) => ' [' + genre.name + '] ')}
                {<br />}
                {<br />}
                {<b>Platforms: </b>}
                {GAME?.platforms.map((pla: any) => ' [' + pla.platform.name + '] ')}
                {<br />}
                {<br />}
                {<b>Description:</b>} {GAME?.description_raw}
                {<br />}
                {<br />}
              </div>
            }
            {
              <button
                className="btn btn-outline-primary btn-md px-3 text-dark"
                onClick={() => {
                  SHOWREVIEWS == true ? setShowReviews(false) : setShowReviews(true);
                }}
              >
                {SHOWREVIEWS == true
                  ? `Show reviews: (${REVIEWS.length})`
                  : `Hide reviews: (${REVIEWS.length})`}
              </button>
            }

            {<br />}
            {<br />}
            <div hidden={!SHOWREVIEWS}>
              {' '}
              <Card title={`Top review`}>
                {<h3> {TOPREVIEW?.title} </h3>}

                {<p> {TOPREVIEW?.review} </p>}
              </Card>
            </div>

            <div hidden={SHOWREVIEWS}>
              {REVIEWS.map((rev: Review, index: number) => (
                <Card key={rev.review_id} title={`${rev.title}`}>
                  <Row></Row>
                  <Row>
                    <Column>
                      {rev.rating == 1 ? (
                        <Icon.Dice1 />
                      ) : rev.rating == 2 ? (
                        <Icon.Dice2 />
                      ) : rev.rating == 3 ? (
                        <Icon.Dice3 />
                      ) : rev.rating == 4 ? (
                        <Icon.Dice4 />
                      ) : rev.rating == 5 ? (
                        <Icon.Dice5 />
                      ) : rev.rating == 6 ? (
                        <Icon.Dice6 />
                      ) : (
                        'Unrated'
                      )}
                    </Column>
                    <Column right>
                      <div hidden={!user.data?.user}>
                        <Icon.Like
                          liked={REVIEWINFO[index]?.h == true ? true : false}
                          onClick={() => {
                            if (HIDDENHELPFUL == 'Unlock') {
                              updateHelpful(rev, index);
                            }
                          }}
                        />

                        {rev.helpful}

                        <Icon.DisLike
                          dislike={REVIEWINFO[index]?.n == true ? true : false}
                          onClick={() => {
                            if (HIDDENNOTHELPFUL == 'Unlock') {
                              updateNotHelpful(rev, index);
                            }
                          }}
                        />
                        {rev.not_helpful}
                      </div>
                    </Column>
                  </Row>

                  <Column> {rev.review}</Column>

                  <div hidden={user.data?.user?.username != rev.user_name}>
                    <Column right>
                      {' '}
                      <Icon.Delete
                        onClick={() => {
                          deleteReview(rev.review_id);
                        }}
                      />
                    </Column>

                    <Column right>
                      {' '}
                      <div hidden={user.data?.user?.username != rev.user_name}>
                        <Icon.Edit
                          hidden={!rev.state}
                          onClick={() => {
                            var array = [...REVIEWS];
                            array[index].state = false;
                            setReviews(array);
                          }}
                        />
                      </div>
                      <div hidden={user.data?.user?.username != rev.user_name}>
                        <Icon.Edit
                          hidden={rev.state}
                          onClick={() => {
                            var array = [...REVIEWS];
                            array[index].state = true;
                            setReviews(array);
                          }}
                        />
                      </div>
                    </Column>
                  </div>

                  <Column right>{rev.user_name}</Column>
                  <br />
                  {
                    <div hidden={REVIEWS[index].state}>
                      <div hidden={user.data?.user?.username != rev.user_name}>
                        {
                          <div>
                            <form>
                              <div>
                                Title:
                                <Form.Input
                                  type={'text'}
                                  value={rev.title}
                                  onChange={(event) => {
                                    var array = [...REVIEWS];
                                    array[index].title = event.currentTarget.value;
                                    setReviews(array);
                                  }}
                                ></Form.Input>
                              </div>

                              <div>
                                Review:
                                <Form.Textarea
                                  value={rev.review}
                                  onChange={(event) => {
                                    var array = [...REVIEWS];
                                    array[index].review = event.currentTarget.value;
                                    setReviews(array);
                                  }}
                                ></Form.Textarea>
                              </div>

                              <div>
                                <Button.Success
                                  onClick={() => {
                                    connected == true
                                      ? connection.current?.send('Review Updated')
                                      : null;

                                    databaseservice.updateReview(rev).then(() => {
                                      var array = [...REVIEWS];
                                      array[index].state = true;
                                      setReviews(array);
                                    });
                                  }}
                                >
                                  Submit
                                </Button.Success>
                              </div>
                            </form>
                          </div>
                        }
                      </div>
                    </div>
                  }
                </Card>
              ))}
              <br />
              <br />
            </div>
          </Card>
        </div>
      </>
    );
  }
};

export default GameDetails;
