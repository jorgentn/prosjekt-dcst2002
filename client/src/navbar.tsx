import React from 'react';
import { useHistory } from 'react-router-dom';
import NavbarSearch from './navbar-search';
import { useUser } from './login';
import loginservice from './LoginService';

const NavBar = (props: any) => {
  let history = useHistory();
  const user: any = useUser();

  return (
    <div className="">
      <nav className="navbar fixed-top navbar-expand-md navbar-dark bg-dark ">
        <div className="logo text-white">
          <button
            className=""
            style={{ background: 'none', border: 'none', color: 'white' }}
            onClick={() => {
              if (props.props.match.path == '/') {
                props.setSearched({ data: [], status: false });
              } else {
                window.location.href = '/';
              }
            }}
          >
            GameRating
          </button>
          <button className="btn btn-outline-primary btn-sm px-3">
            <a target="_blank" className="text-white" href="https://rawg.io/games/create">
              Add game
            </a>
          </button>
        </div>

        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarNav"
          aria-controls="navbarButton"
          aria-expanded="false"
          aria-label="Toggel navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <div className="container d-flex justify-content-around">
            {<div>{props.setSearched ? <NavbarSearch setSearched={props.setSearched} /> : ''}</div>}

            <div className="login d-flex">
              {
                <div>
                  <button
                    hidden={!user?.data?.isAuthenticated}
                    type="button"
                    className="btn btn-outline-primary btn-md px-3 text-white"
                    onClick={() => {
                      loginservice.logout().then(() => window.location.reload());
                    }}
                  >
                    Logout
                  </button>
                </div>
              }
              {
                <button
                  hidden={user?.data?.isAuthenticated}
                  type="button"
                  className="btn btn-outline-primary btn-md px-3 text-white"
                  onClick={() => {
                    history.push('/login');
                  }}
                >
                  Login
                </button>
              }
              {
                <p
                  className={'text-white'}
                  style={{ marginTop: '8px', marginLeft: '4px', marginBottom: '8px' }}
                >
                  {user?.data?.user?.username}
                </p>
              }
            </div>
          </div>
        </div>
      </nav>

      <br />
      <br />
      <br />
    </div>
  );
};

export default NavBar;
