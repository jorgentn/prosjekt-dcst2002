import React, { useState, useEffect } from 'react';
import { NavLink } from 'react-router-dom';
import { Card, Row } from './widgets';
import NavBar from './navbar';
import gameservice from './game-service';
import SearchTitle from './search-title';
import databaseservice from './database-service';
import { useUser } from './login';
import type { Game, Rating } from './types/types';

const Home = (props?: any) => {
  const initialState = () => [];
  const [GAMES, setGames] = useState(initialState);
  const [isLoading, setisLoading] = useState(false);
  const [ERROR, setError] = useState(false);
  const [Searched, setSearched] = useState({ data: [], status: false });
  const [RATING, setRating] = useState([]);
  const user = useUser();
  const [USER, setUser] = useState(user);

  useEffect(() => {
    setisLoading(true);

    gameservice
      .getAll('none', 'all')
      .then((response) => {
        setGames(response.results);

        setisLoading(false);
      })

      .catch((error) => {
        console.error(error);
        setError(true);
      });

    databaseservice.getRatings().then((response) => setRating(response.data));
  }, [USER]);

  let listOfGames = GAMES.map((game: Game) => {
    return (
      <div
        className="col-md-2"
        key={game.id}
        style={{
          margin: 'auto',
          width: 'auto',
          marginBottom: '7rem',
        }}
      >
        <div className="game-box">
          <NavLink to={`/game/${game.id}`} style={{ textDecoration: 'none' }}>
            <div className="game-inhold">
              <Card title={game.name}>
                {<img src={game.background_image}></img>}

                {RATING?.map((rating: Rating) => {
                  if (rating.id == game.id) {
                    return (
                      'Rating: ' + (rating.rating_total / rating.rating_count).toFixed(2) + ' /6'
                    );
                  }
                })}
              </Card>
            </div>
          </NavLink>
        </div>
      </div>
    );
  });

  if (ERROR) {
    return <div className="text-center">Error fetching data </div>;
  } else if (isLoading) {
    return (
      <div>
        <div className="loading-text">
          <p style={{ whiteSpace: 'nowrap' }}>Loading games...</p>
        </div>
        <div className="loading">
          <div className="spinner">
            <div className="mask">
              <div className="maskedCircle"></div>
            </div>
          </div>
        </div>
      </div>
    );
  } else if (Searched.status == true) {
    return (
      <div>
        <SearchTitle
          searched={Searched}
          setSearched={setSearched}
          RATING={RATING}
          homeProps={props}
        />
      </div>
    );
  } else {
    return (
      <div className="bg">
        <NavBar setSearched={setSearched} props={props} />
        <Row>{listOfGames}</Row>
      </div>
    );
  }
};

export default Home;
