import React, { useState } from 'react';
import gameservice from './game-service';

const NavbarSearch = (params: any) => {
  const [SØK, setSøk] = useState('');
  const [PLATFORM, setPlatform] = useState('all');

  return (
    <div>
      <div>
        <div className="d-flex" id="#navbarNav">
          <input
            className="col-xs-4 form-control rounded"
            placeholder="Søk etter game"
            type={'text'}
            value={SØK}
            onChange={(event) => {
              setSøk(event.currentTarget.value);
            }}
          ></input>

          <button
            className="btn btn-outline-primary btn-md px-3 text-white"
            onClick={() => {
              gameservice.getAll(SØK, PLATFORM).then((res) => {
                if (SØK.length != 0 || PLATFORM != 'all')
                  params.setSearched({ data: res.results, status: true });
                else params.setSearched({ data: [], status: false });
              });
            }}
          >
            Søk
          </button>

          <select
            className="select "
            aria-label=".form-select example"
            onChange={(event) => {
              setPlatform(event.currentTarget.value);
            }}
          >
            <option value="all">All</option>
            <option value="4">PC</option>
            <option value="187">PS5</option>
            <option value="18">PS4</option>
            <option value="16">PS3</option>
            <option value="1">Xbox one</option>
            <option value="14">xbox 360</option>
            <option value="7">Nintendo switch</option>
          </select>
        </div>
      </div>
    </div>
  );
};

export default NavbarSearch;
