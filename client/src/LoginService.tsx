import axios from 'axios';

class LoginService {
  register = (registerUsername: String, registerPassword: String) => {
    return axios({
      method: 'POST',
      data: {
        username: registerUsername,
        password: registerPassword,
      },
      withCredentials: true,
      url: '/register',
    });
  };
  login = (loginUsername: String, loginPassword: String) => {
    return axios({
      method: 'POST',
      data: {
        username: loginUsername,
        password: loginPassword,
      },
      withCredentials: true,
      url: '/login',
    }).then((res) => res);
  };

  logout = () => {
    return axios({
      method: 'GET',
      data: {},
      withCredentials: true,
      url: '/logout',
    }).then((res) => res);
  };
  getUser = () => {
    return axios({
      method: 'GET',
      withCredentials: true,
      url: '/user',
    }).then((res) => res.data);
  };
}

const loginservice = new LoginService();

export default loginservice;
